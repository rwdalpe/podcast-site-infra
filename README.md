# Podcast Site Infrastructure Repository

Configuration management and script resources for the podcast site. Currently
only really useful for initial configuration and non-MediaWiki related upgrades.
The scripts are pretty harsh on the installation, so upgrading via this isn't
recommended.

## Usage

1. Install salt on target machine: <http://repo.saltstack.com/>
2. Clone this repository onto target machine
3. Navigate into the `salt` directory
4. Execute `sudo salt-call -c . -l info state.apply`
5. Secure the mariadb installation by running `mysql_secure_installation`
6. Execute `/var/www/db_setup.sh`
7. Navigate to the site to set up Wordpress if necessary.
8. Navigate to the site at the `/wiki` path to set up MediaWiki if necessary.

## Extra Notes

Will attempt to gracefully handle situations where LetsEncrypt has already been
run on a machine set up using these steps.