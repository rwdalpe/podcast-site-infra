#!yaml|gpg

backup_aws_key: | 
    -----BEGIN PGP MESSAGE-----

    hQEMAxx97ruDGHDEAQf6An2mZGAHeO1kPgnhRzDRVrYvQq6zxChdOq/NHgoCx5TP
    LBqaIQ9KrloYQ6pgOgzlqJUwmoQECIAQLAEXYKO2a7LrkVUutvJKIP4SBy+va/5a
    AYzKs88gakduVqADylazaKIN9YblHJciQicxzhCluRvjsaTmBy7hiUlClsjW1+gG
    xyteEX17YA+19tBOcwAbMuDsiXCAQ6aeJyDz/+p2BQvsLBCeompC/wS2VExCZSx7
    O/tUEEV377aL3uk+2zSPGqCxYl+rUTQe10ZVjkeT22sIGp4JtYAUKb/X/uUNktuX
    HLcFZGsCha/RP+joN496sdbwVqGtsXyD3hDfSyDtjdJPAStYpFsOJxOlmljuuras
    NMQam+NY6U7YFxOLx+D29j3hRvvUyeHkhcnDHOsxvOTAIwLLpqbEFudg2XcCpcaI
    egP6kCmZ+OymWSfmrCSt2g==
    =XA4b
    -----END PGP MESSAGE-----
backup_aws_secret: |
    -----BEGIN PGP MESSAGE-----

    hQEMAxx97ruDGHDEAQf8DJKxIW6yp6iDz+x37CgoTWgkpWk4L7RCvRPub6uuqNNy
    XGRzrCD/vEoSOxl3iKbusQRrGa07l5jQUkOlCBBiXb1hsA+vrrvoiQPsg42TW1xo
    1BJWT4JbSWEkw6SVqwRncqKUu+B4DulOOXEcNj4GJF3PtphAkIhVAKRbcBQzq5WA
    tS1/keV4baQsuf9M/YhxzKOtae/N2Sqd63ozM9sQZqALMQ/OoCgZu5oSrkv4GSUF
    e1iTJqnLvtWCeeKAlKjiDGj1H+M249EePvNF+bLBi+o6tVU0f7SikylRX8qfeVXX
    YHe2g1n0aM3ChOBtquQvEWdfN0P1lCkh6cPajjx6cdJjAfJrqdSnYRI0Q41ZfnmV
    GLVJ5eqQNGZxUt4grth6uYL2YIBPmfDvQqMcg+R9R4MlY8IF9xXCybIQpRmJVJWH
    ctsMpETelauNnVgEJyIjHXdkS/KFbSOWIvrGzA3Jj1YmhaZl
    =sez4
    -----END PGP MESSAGE-----
