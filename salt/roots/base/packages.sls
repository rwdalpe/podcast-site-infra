{% for package in ['apache2',
    'php',
    'libapache2-mod-php7.0',
    'imagemagick',
    'mariadb-server',
    'php7.0-mysql',
    'php7.0-xml',
    'php7.0-mbstring',
    'python-letsencrypt-apache',
    'python-pip'] %}
{{ package }}:
    pkg.installed
{% endfor %}

awscli:
    pip.installed
