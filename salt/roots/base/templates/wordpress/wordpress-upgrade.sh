#!/bin/bash

set -e
set -u

WORDPRESS_VERSION=$1
WORDPRESS_TAR=$2
WORDPRESS_DIR=/var/www/blog

systemctl stop apache2

rm -rf "${WORDPRESS_DIR}/wp-admin"
rm -rf "${WORDPRESS_DIR}/wp-includes"

tar --directory="${WORDPRESS_DIR}/" --strip-components=1 -xvf "${WORDPRESS_TAR}"

echo $WORDPRESS_VERSION > "${WORDPRESS_DIR}/version.txt"

systemctl start apache2