#!/bin/bash

set -u
set -e

export AWS_ACCESS_KEY_ID='{{ pillar.get('backup_aws_key') }}'
export AWS_SECRET_ACCESS_KEY='{{ pillar.get('backup_aws_secret') }}'
CURYEAR=$(date +%Y)
CURMONTH=$(date +%m)

echo "Starting to back up DBs"
/usr/local/bin/aws s3 sync /root/sqlbackups/ s3://rwdalpe-podcast-backup/$CURYEAR/$CURMONTH/
rm -vf /root/sqlbackups/*

echo "Starting to back up Wiki media"
/usr/local/bin/aws s3 sync /var/www/wiki/images/ s3://rwdalpe-podcast-backup/wiki-media/