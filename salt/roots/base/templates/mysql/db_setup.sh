#!/bin/sh

set -e
set -u

MYSQL_CMD="mysql -u root"
MYSQL_CMD_FILE="/tmp/.db_setup_sql_script.sql"

rm -fv "${MYSQL_CMD_FILE}"
touch "${MYSQL_CMD_FILE}"
chown -v root:root "${MYSQL_CMD_FILE}"
chmod -v 0600 "${MYSQL_CMD_FILE}"
echo "" > "${MYSQL_CMD_FILE}"

echo "REMINDER: Please run the 'mysql_secure_installation' script"
echo "to make the database installation slightly more secure and "
echo "set a root password if you haven't already!'"

echo

echo "SELECT 'Creating wordpress database' as '';" >> "${MYSQL_CMD_FILE}"
echo "CREATE DATABASE wordpress;" >> "${MYSQL_CMD_FILE}"
echo "SELECT 'Done creating wordpress database' as '';" >> "${MYSQL_CMD_FILE}"

echo

echo "SELECT 'Creating wiki database' as '';" >> "${MYSQL_CMD_FILE}"
echo "CREATE DATABASE wiki;" >> "${MYSQL_CMD_FILE}"
echo "SELECT 'Done creating wiki database' as '';" >> "${MYSQL_CMD_FILE}"

echo

echo "SELECT 'Creating wordpress database user: wordpress' as '';" >> "${MYSQL_CMD_FILE}"
echo -n "Please enter a password for the wordpress database user: "
stty -echo
read WORDPRESS_PW
stty echo
echo
echo "CREATE USER 'wordpress' IDENTIFIED BY '${WORDPRESS_PW}';" >> "${MYSQL_CMD_FILE}"
echo "SELECT 'Done creating wordpress database user: wordpress' as '';" >> "${MYSQL_CMD_FILE}"

echo

echo "SELECT 'Creating wiki database user: wiki' as '';" >> "${MYSQL_CMD_FILE}"
echo -n "Please enter a password for the wiki database user"
stty -echo
read WIKI_PW
stty echo
echo
echo "CREATE USER 'wiki' IDENTIFIED BY '${WIKI_PW}';" >> "${MYSQL_CMD_FILE}"
echo "SELECT 'Done creating wiki database user: wiki' as '';" >> "${MYSQL_CMD_FILE}"

echo

echo "SELECT 'Granting wordpress database privileges to user: wordpress' as '';" >> "${MYSQL_CMD_FILE}"
echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'%' WITH GRANT OPTION;" >> "${MYSQL_CMD_FILE}"
echo "SELECT 'Done granting wordpress database privileges to user: wordpress' as '';" >> "${MYSQL_CMD_FILE}"

echo

echo "SELECT 'Granting wiki database privileges to user: wiki' as '';" >> "${MYSQL_CMD_FILE}"
echo "GRANT ALL PRIVILEGES ON wiki.* TO 'wiki'@'%' WITH GRANT OPTION;" >> "${MYSQL_CMD_FILE}"
echo "SELECT 'Done granting wiki database privileges to user: wiki' as '';" >> "${MYSQL_CMD_FILE}"

echo

$MYSQL_CMD < "${MYSQL_CMD_FILE}"

rm -fv "${MYSQL_CMD_FILE}"

echo "Complete"