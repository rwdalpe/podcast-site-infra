#!/bin/bash

set -e
set -u

MEDIAWIKI_VERSION=$1
MEDIAWIKI_PACKAGE=$2
WWW_DIR=/var/www
UPGRADE_DIR="${WWW_DIR}/mediawiki-upgrade"
WIKI_DIR="${WWW_DIR}/wiki"

systemctl stop apache2

mkdir -v "${UPGRADE_DIR}" 
tar --directory="${UPGRADE_DIR}/" --strip-components=1 -xvf "${MEDIAWIKI_PACKAGE}"

cp -Rvf "${WIKI_DIR}/images" "${UPGRADE_DIR}/"
chown -R www-data:www-data "${UPGRADE_DIR}/images"

cp -vf "${WIKI_DIR}/LocalSettings.php" "${UPGRADE_DIR}/LocalSettings.php"

rm -rf "${WIKI_DIR}"

mv -vf "${UPGRADE_DIR}" "${WIKI_DIR}"

cd "${WIKI_DIR}/maintenance"
php "update.php"

echo $MEDIAWIKI_VERSION > "${WIKI_DIR}/version.txt"

systemctl start apache2