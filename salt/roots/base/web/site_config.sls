#!py

import os.path

def run():
    config = {}

    siteconf = '/etc/apache2/sites-available/001-podcast.conf'
    le_siteconf_exists = os.path.isfile(siteconf.replace(".conf", "-le-ssl.conf"))

    config[siteconf] = {
        'file.managed': [
            { 'source': 'salt://templates/apache2/sites-available/001-podcast.conf' },
            { 'template': 'py' },
            { 'context': 
                {
                    'le_site_exists': le_siteconf_exists
                }
            },
            { 'watch_in': 
                [
                    { 'service': 'apache2 service start' }
                ]
            }
        ]
    }

    config['Enable podcast site'] = {
        'apache_site.enabled': [
            {'name': '001-podcast' },
            {'watch_in': 
                [
                    { 'service' : 'apache2 service start' }
                ]
            }
        ]
    }

    if le_siteconf_exists:
        config['Enable LetsEncrypt podcast site'] = {
            'apache_site.enabled': [
                {'name': '001-podcast-le-ssl' },
                {'watch_in': 
                    [
                        { 'service' : 'apache2 service start' }
                    ]
                }
            ]
        }
    else:
        config['Disable LetsEncrypt podcast site'] = {
            'apache_site.disabled': [
                {'name': '001-podcast-le-ssl' },
                {'watch_in': 
                    [
                        { 'service' : 'apache2 service start' }
                    ]
                }
            ]
        }

    return config
