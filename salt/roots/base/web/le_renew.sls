#!py

import os.path

def run():
    config = {}

    if os.path.isdir("/etc/letsencrypt/"):
        config['LE renew cronjob'] = {
            'cron.present': [
                { 'name': 'letsencrypt renew' },
                { 'minute': 0 },
                { 'hour': '7,19' },
            ]
        }
    
    return config