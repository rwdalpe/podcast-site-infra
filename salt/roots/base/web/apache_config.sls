Disable apache2 default site:
    apache_site.disabled:
        - name: 000-default
        - watch_in:
            - service: apache2 service start

Enable php7 module:
    apache_module.enabled:
        - name: php7.0
        - watch_in:
            - service: apache2 service start