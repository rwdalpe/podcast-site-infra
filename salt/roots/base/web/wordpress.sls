Download wordpress file:
    file.managed:
        - name: /var/www/wordpress.tar.gz
        - source: https://wordpress.org/wordpress-4.7.4.tar.gz
        - source_hash: sha1=153592ccbb838cafa1220de9174ec965df2e9e1a

Execute wordpress installation:
    cmd.run:
        - cwd: /var/www/
        - unless: test -d blog && grep blog/version.txt -e "4.7.4"
        - name: "rm -rf ./blog && tar xf ./wordpress.tar.gz && mv ./wordpress blog && echo '4.7.4' > ./blog/version.txt"
        - watch_in:
            - service: apache2 service start

Wordpress upgrade script:
    file.managed:
        - name: /var/www/wordpress-upgrade.sh
        - source: salt://templates/wordpress/wordpress-upgrade.sh
        - user: root
        - group: root
        - mode: 0700