Download mediawiki file:
    file.managed:
        - name: /var/www/mediawiki.tar.gz
        - source: https://releases.wikimedia.org/mediawiki/1.28/mediawiki-1.28.1.tar.gz 
        - source_hash: sha256=69d15d2403304837f5afb32dc5a14d369f9ebcdb6206a91b7ec272ceab2c76e5

Execute mediawiki installation:
    cmd.run:
        - cwd: /var/www/
        - unless: test -d wiki && grep wiki/version.txt -e "1.28.1"
        - name: "rm -rf ./wiki && tar xf ./mediawiki.tar.gz && mv ./mediawiki-* wiki && echo '1.28.1' > ./wiki/version.txt"
        - watch_in:
            - service: apache2 service start

/var/www/wiki/images:
    file.directory:
        - user: www-data
        - group: www-data
        - mode: 0755

mediawiki upgrade script:
    file.managed:
        - name: /var/www/mediawiki-upgrade.sh
        - source: salt://templates/wiki/mediawiki-upgrade.sh
        - user: root
        - group: root
        - mode: 0700