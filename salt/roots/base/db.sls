/var/www/db_setup.sh:
    file.managed:
        - source: salt://templates/mysql/db_setup.sh
        - user: root
        - group: root
        - mode: 0700

/etc/mysql/my.cnf:
    file.managed:
        - source: salt://templates/mysql/my.cnf
        - watch_in:
            - service: mysql

mysql:
    service.running:
        - enable: True

database backups dir:
    file.directory:
        - name: /root/sqlbackups
        - user: root
        - group: root
        - dir_mode: 0600
        - 
backup upload script:
    file.managed:
        - name: /root/backupupload.sh
        - source: salt://templates/mysql/backupupload.sh
        - user: root
        - group: root
        - mode: 0700
        - template: jinja

wordpress backup job:
    cron.present:
        - name: mysqldump -u root wordpress > /root/sqlbackups/wordpress_$(date +\%Y\%m\%d_\%H\%M).sql
        - minute: 0
        - hour: 1

wiki backup job:
    cron.present:
        - name: mysqldump -u root wiki > /root/sqlbackups/wiki_$(date +\%Y\%m\%d_\%H\%M).sql
        - minute: 0
        - hour: 1

aws s3 backup upload job:
    cron.present:
        - name: /bin/bash /root/backupupload.sh > /root/backupuploadl.log 2>&1
        - minute: 0
        - hour: 12